<?php
require_once(__DIR__ . '/../env.php');
$yii = __DIR__ . '/../yii/framework/' . (APP_ENV == 'prod' ? 'yiilite' : 'yii') . '.php';
$config = __DIR__ . '/../protected/config/main.php';
require_once($yii);
$app = Yii::createWebApplication($config);
$app->run();