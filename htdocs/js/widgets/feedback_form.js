PX.define('widgets.feedback-form', function (window, PX, $) {

    var self,
        $widget,
        $feedbackForm,
        $feedbackFormSuccess,
        $closeFeedbackFormButton,
        $inputs,
        $inputText,
        $submit;

    return {
        init: function (data, $el) {
            $widget = $el;
            self = this;
            $feedbackForm = $widget.find('.feedback-form');
            $feedbackFormSuccess = $widget.find('.form-feedback-success');
            $closeFeedbackFormButton = $feedbackForm.find('.remodal-close');
            $inputs = $feedbackForm.find('input, textarea');
            $inputText = $feedbackForm.find('.error-msg');
            $submit = $el.find('.js-feedback-submit');

            self.restoreState();
            $submit.on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    method: "POST",
                    url: "/site/feedbackForm",
                    data: $feedbackForm.serialize(),
                    success: function(resp) {
                        console.log(resp);
                        self.clearInputErrors();
                        if (resp.errors) {
                            $inputText.show();
                            for ( var error in resp.errors) {
                                $feedbackForm.find('.' + error).addClass('error');
                            }
                        }

                        if (resp.status === true) {
                            $feedbackForm.hide();
                            $feedbackFormSuccess.show();
                            setTimeout(function() {
                                window.location = window.location.href.split("#")[0];
                            }, 2000);
                        }
                    }
                });
            });
        },
        clearInputErrors: function() {
            $inputs.removeClass('error');
            $inputText.hide();
        },
        restoreState: function() {
            $feedbackForm.show();
            $feedbackFormSuccess.hide();
        }
    };
});
