PX.define('widgets.regions-map', function (window, PX, $) {

    var el = 'map';
    var width = 800,
        height = 462;

    var self,
        $body,
        $regionsMap,
        $mapResultArea,
        $loader;

    return {
        init: function (data, $el) {
            self = this;
            self.initMap();
            $body = $('body');

            $regionsMap = $body.find('.maps');
            $mapResultArea = $body.find('.js-mapResultData');
            $loader = $mapResultArea.find('#loader');

            $regionsMap.on('mouseover', 'a', function() {
                $(this).css('opacity', 0.7);
            });

            $regionsMap.on('mouseout', 'a', function() {
                $(this).css('opacity', 1);
            });

            $regionsMap.on('click', 'a path' ,function(e) {
                e.preventDefault();
                var regionId = $(this).parent('a').attr('href');
                self.getRegionDataById(regionId);
            });
        },
        initMap: function() {
            var rsr = Raphael(el, width, height);
            rsr.setViewBox(0, 0, 64426, 36403, true);
            $.getJSON( "/json/getMapData", function( regions ) {
                regions.forEach(function(region) {
                    rsr.path(region.coords)
                        .attr(region.attributes)
                        .toBack();
                });
            });
        },
        getRegionDataById: function(regionId) {
            $loader.show();
            regionId = regionId || 45000; // Москва

            $.ajax({
                method: "GET",
                url: "/json/getRegionById",
                data: {
                    regionId: regionId
                },
                success: function(resp) {
                    $mapResultArea.html(resp.html);
                    $loader.hide();
                }
            });
        }
    };
});
