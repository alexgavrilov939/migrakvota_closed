/**
 *  Core of Migrakvota Javascript library
 */
(function(window, $, undefined) {
    var document = window.document,
        emptyFn = function() {},
        debug = emptyFn,
        debugError = debug,
        args = {};

    if (!Array.isArray) {
        Array.isArray = function(vArg) {
            return Object.prototype.toString.call(vArg) === '[object Array]';
        };
    }

    // Parse core arguments
    (function() {
        'use strict';
        // Fetch base URL and script arguments
        var scripts = document.getElementsByTagName('script'),
            i, c = scripts.length,
            src, pos;
        for (i = 0; i < c; i++) {
            src = scripts[i].getAttribute('src');
            if (src && (
                    ((pos = src.indexOf('/core.js')) != -1) || ((pos = src.indexOf('/all.js')) != -1)
                )) {
                if ((pos = src.indexOf('?')) != -1) {
                    args = parseQueryString(src.substring(pos + 1));
                }
                break;
            }
        }
    })();

    // Set debug function
    /* jshint ignore:start */
    if (args.debug == 1) {
        debug = function() {
            console.log.apply(console, arguments);
        };

        debugError = function() {
            console.log.apply(console, arguments);
        };
    }
    /* jshint ignore:end */

    /**
     * Global PX object
     * @type {Object}
     */
    window.PX = {
        modules: {},
        define: defineModule,
        module: initModule,
        processWidgets: processWidgets,
        debug: debug,
        debugError: debugError,
        m: m,
        w: w,
        global: {},
        events: $({}),
        addGlobal: function(key, value) {
            this.global[key] = value;
        }
    };

    /**
     * Defines module and gets params to local module storage
     * @param id
     * @param callback
     */
    function defineModule(id, callback) {
        if (PX.modules[id] === undefined) {
            var module = {
                inited: false,
                requires: []
            };
            module = $.extend(module, callback(window, PX, $, undefined));
            PX.modules[id] = module;
            //debug('module ' + id + ' is loaded');
        }
    }

    /**
     * Gets all .widget elements and init widgets
     * @param el
     */
    function processWidgets(el) {
        var $el = $(el);
        var $widgets = $el.find('.widget-px');
        if ($el.hasClass('widget-px')) {
            $widgets = $widgets.add($el);
        }

        $widgets.each(function() {
            var widgetId = this.getAttribute('data-widget');
            var params, paramsRaw;

            if (widgetId) {
                paramsRaw = this.getAttribute('data-params');
                params = (paramsRaw) ? parseQueryString(paramsRaw) : {} ;
                PX.module(widgetId, params, this, false);
            } else if (this.onclick) {
                params = this.onclick();
                params.data = (params.data === undefined) ? {} : params.data;
                this.onclick = null;
                if (params.id) {
                    PX.module(params.id, params.data, this, false);
                }
            }
        });
    }

    /**
     * Initialize module
     *
     * @param id
     * @param data
     * @param self
     * @param cache
     * @return {*}
     */
    function initModule(id, data, self, cache) {
        cache = (cache === undefined) ? true : cache;
        var module = PX.modules[id];
        if (module !== undefined) {
            if (!module.inited || !cache) {
                if (module.requires !== undefined && $.isArray(module.requires) && module.requires.length) {
                    debug('module ' + id + ' requires ' + module.requires.join(', '));
                    for (var i in module.requires) {
                        initModule(module.requires[i]);
                    }
                }

                // exec init if there is data, self and init method
                if (data !== undefined && self !== undefined && module.init !== undefined && $.isFunction(module.init)) {
                    module.init(data, $(self));
                    //debug('module ' + id + ' is inited');
                }

                module.inited = true;
                PX.modules[id] = module;
            }

            if (cache) {
                PX.modules[id] = module;
                return PX.modules[id];
            } else {
                return module;
            }

        } else {
            var msg = 'module ' + id + ' is not defined';
            debugError(msg);
            throw msg;
        }
    }

    /**
     * Shortcut for widgets.*
     *
     * @param id
     * @return {*}
     */
    function w(id) {
        return initModule('widgets.' + id);
    }

    /**
     * Shortcut for modules.*
     *
     * @param id
     * @return {*}
     */
    function m(id) {
        return initModule('modules.' + id);
    }

    /**
     * Parses query string
     * @param qs
     * @return {Object}
     */
    function parseQueryString(qs) {
        'use strict';
        var vars = qs.split('&'),
            c = vars.length,
            res = {},
            i;

        for (i = 0; i < c; i++) {
            var pair = vars[i].split('=');
            res[pair[0]] = pair[1];
        }
        return res;
    }

    $(function() {
        processWidgets(document);
    });

    window.onerror = function myErrorHandler(errorMsg, errorUrl, errorLine, errorChar, errorType) {

        if ((errorUrl !== undefined) && (errorUrl !== '') && (/(.*migrakvota.gov.ru)/i.test(PX.m('helpers').parseUri(errorUrl).host))) {
            debugError(errorMsg, [errorUrl, errorLine, errorChar, errorType]);

            $.ajax({
                type: 'POST',
                url: '/site/errorlog/',
                data: {
                    'msg': 'msg: ' + errorMsg + ', script: ' + errorUrl + ', url: ' + window.location.href + ', line: ' + errorLine,
                    'category': 'js'
                },
                cache: false,
                success: function() {
                    debug('error sent');
                },
                error: function(result, httpCode, jqXHR) {
                    debug('error', result, httpCode, jqXHR);
                }
            });
        }

        return false;
    };

})(window, jQuery);