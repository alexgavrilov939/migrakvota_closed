'use strict';

var
    chalk           = require('chalk'),
    gulp            = require('gulp'),
    browserSync     = require('browser-sync').create(),
    clean           = require('gulp-clean'),
    plumber         = require('gulp-plumber'),
    gulpif          = require('gulp-if'),
    uglify          = require('gulp-uglify'),
    concat          = require('gulp-concat'),
    minifyCss       = require('gulp-minify-css'),
    autoprefix      = require('gulp-autoprefixer'),
    sourcemaps      = require('gulp-sourcemaps'),
    replace         = require('gulp-replace'),
    merge           = require('merge-stream'),
    sequence        = require('run-sequence'),
    babel           = require('gulp-babel'),

    flags           = require('minimist')(process.argv.slice(2)),
    isProduction    = flags.production || flags.prod || false,
    needSourcemaps  = flags.sourcemaps || flags.sm || false,
    watching        = flags.watch || false
;


// BUILD ------------------------------------------------------------------------ //

gulp.task('build', function(callback) {

    console.log(chalk.green('Building ' + (isProduction ? 'production' : 'dev') + ' version...'));

    if (flags.watch) {
        sequence(
            'clean',
            [
                'js',
                'css'
            ],
            'watch',
            function() {
                callback();
                console.log(chalk.green('Big brother is watching you...'))
            }
        )
    } else {
        sequence(
            'clean',
            [
                'js',
                'css'
            ],
            function() {
                callback();
                console.log(chalk.green('✔ All done!'))
            }
        )
    }
});


// SCRIPTS ------------------------------------------------------------------------ //

gulp.task('js', function () {
    var core = gulp.src([
        'htdocs/js/core.js',
        'htdocs/js/widgets/**/*.js'
    ])
        .pipe(plumber())
        .pipe(babel())
        .pipe(gulpif(needSourcemaps, sourcemaps.init()))
        .pipe(concat('app.js'))
        .pipe(gulpif(isProduction, replace(/(\/\/)?(console\.)?log\((.*?)\);?/g, '')))
        .pipe(gulpif(isProduction, uglify()))
        .pipe(gulpif(needSourcemaps, sourcemaps.write('../maps')))

        .pipe(gulp.dest('htdocs/assets/build/js'));

    var vendor = gulp.src([
        'htdocs/vendor/jquery/dist/jquery.js',
        'htdocs/vendor/raphael/js/raphael-min.js',
        'htdocs/vendor/remodal/js/zepto.js',
        'htdocs/vendor/remodal/js/remodal.js'
    ])
        .pipe(plumber())
        .pipe(gulpif(needSourcemaps, sourcemaps.init()))
        .pipe(concat('app.vendor.js'))
        .pipe(gulpif(isProduction, uglify()))
        .pipe(gulp.dest('htdocs/assets/build/js'));

    return merge(core, vendor);
});


// STYLUS ------------------------------------------------------------------------ //

gulp.task('css', function () {
    var core = gulp.src([
        'htdocs/css/reset.css',
        'htdocs/css/inline-content.css',
        'htdocs/css/fonts.css',
        'htdocs/css/main.css'
    ])
        .pipe(plumber())
        .pipe(gulpif(needSourcemaps, sourcemaps.init()))
        .pipe(autoprefix('last 2 version', 'ie 8', 'ie 9'))
        .pipe(concat('app.css'))
        .pipe(gulpif(isProduction, minifyCss()))
        .pipe(gulp.dest('htdocs/assets/build/css'))
        .pipe(gulpif(watching, browserSync.stream()));

    var vendor = gulp.src([
        'htdocs/vendor/raphael/css/maps.css',
        'htdocs/vendor/remodal/css/remodal.css',
        'htdocs/vendor/remodal/css/remodal-default-theme.css'
    ])
        .pipe(plumber())
        .pipe(concat('app.vendor.css'))
        .pipe(gulpif(isProduction, minifyCss()))
        .pipe(gulp.dest('htdocs/assets/build/css'))
        .pipe(gulpif(watching, browserSync.stream()));

    if (watching) {
        browserSync.reload();
    }

    return merge(core, vendor);
});


// CLEAN ------------------------------------------------------------------------ //

gulp.task('clean', function() {
    return gulp.src([
        'htdocs/assets/build/css',
        'htdocs/assets/build/js'
    ])
        .pipe(clean());
});

// WATCH ------------------------------------------------------------------------
gulp.task('watch', function() {
    // Watch .css files
    gulp.watch('htdocs/css/**/*.css', ['css']);

    // Watch .js files
    gulp.watch('htdocs/js/**/*.js', ['js']);

    // Watch .tpl files
    // gulp.watch('protected/views/**/*.tpl', browserSync.reload);
});

// DEFAULT ----------------------------------------------------------------------
gulp.task('default', function() {
    gulp.start('build');
});
