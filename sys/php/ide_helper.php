<?php

throw new Exception('It\'s just IDE auto-complete helper file. Never include it to any project script.');

class Yii
{
    /**
     * @return CApplication
     */
    public static function app()
    {
    }
}

/**
 * @property SiteService site
 * @property Request request
 * @property CWebUser user
 * @property MemcachedPool memcache
 */
abstract class CApplication extends CModule
{
}
