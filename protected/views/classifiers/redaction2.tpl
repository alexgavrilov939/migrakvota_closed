{% extends 'layouts/default.tpl' %}

{% block content %}
    <div class="content l-center">
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a class="breadcrumbs-item__link" href="/">Главная</a>
            </li>
            <li class="breadcrumbs-item">&#8594;</li>
            <li class="breadcrumbs-item">
                <a class="breadcrumbs-item__link" href="{{ this.createUrl('classifiers/index') }}">Классификаторы</a>
            </li>
            <li class="breadcrumbs-item">&#8594;</li>
            <li class="breadcrumbs-item breadcrumbs-item_current">ОКВЭД 2 (ОК 029-2014 (КДЕС Ред. 2))</li>
        </ul>


        <div class="docs-wrapper">
            <table class="docs-table">
                <thead>
                <tr class="docs-table__tr docs-table__thead-tr">
                    <td class="docs-table__tr-td docs-table__thead-tr-td">Код</td>
                    <td class="docs-table__tr-td docs-table__thead-tr-td">Наименование</td>
                </tr>
                </thead>
                <tbody>
                    {% set currentParent = '' %}
                    {% for item in items if items %}
                        {% if item.parent_id == 0 %}
                            <tr class="docs-table__tr docs-table__tr-td_caption">
                                <td class="docs-table__tr-td docs-table__tr-td_caption">
                                    {% if currentParent != item.parent_id and item.parent_id != '0' %}
                                        {{ item.parent_id }}
                                    {% endif %}
                                    {% set currentParent = item.parent_id %}
                                </td>
                                <td class="docs-table__tr-td docs-table__tr-td_caption">{{ item.name }}</td>
                            </tr>
                        {% else %}
                            <tr class="docs-table__tr">
                                <td class="docs-table__tr-td">{{ item.id }}</td>
                                <td class="docs-table__tr-td">{{ item.name }}</td>
                            </tr>
                        {% endif %}
                    {% endfor %}
                </tbody>
            </table>
        </div>
    </div>
{% endblock %}