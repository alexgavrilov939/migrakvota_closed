{% extends 'layouts/default.tpl' %}

{% block content %}
    <div class="content l-center">
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a class="breadcrumbs-item__link" href="/">Главная</a>
            </li>
            <li class="breadcrumbs-item">&#8594;</li>
            <li class="breadcrumbs-item">
                <a class="breadcrumbs-item__link" href="{{ this.createUrl('classifiers/index') }}">Классификаторы</a>
            </li>
            <li class="breadcrumbs-item">&#8594;</li>
            <li class="breadcrumbs-item breadcrumbs-item_current">ОКЗ</li>
        </ul>

        <div class="docs-wrapper">
            <table class="docs-table">
                <thead>
                <tr class="docs-table__tr docs-table__thead-tr">
                    <td class="docs-table__tr-td docs-table__thead-tr-td">Код</td>
                    <td class="docs-table__tr-td docs-table__thead-tr-td">Наименование</td>
                </tr>
                </thead>
                <tbody>
                    {% for item in items if items %}
                        {% set captionClass = (item.check_val == 0) ? 'docs-table__tr-td_caption' : '' %}
                        <tr class="docs-table__tr">
                            <td class="docs-table__tr-td {{ captionClass }}">{{ item.id }}</td>
                            <td class="docs-table__tr-td {{ captionClass }}">{{ item.name }}</td>
                        </tr>
                    {% endfor %}
                </tbody>
            </table>
        </div>
    </div>
{% endblock %}