{% extends 'layouts/default.tpl' %}

{% block content %}
    <div class="content l-center">
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a class="breadcrumbs-item__link" href="/">Главная</a>
            </li>
            <li class="breadcrumbs-item">&#8594;</li>
            <li class="breadcrumbs-item breadcrumbs-item_current">Классификаторы</li>
        </ul>

        <div class="classifiers">
            <div class="caption-xl">Классификаторы</div>
            <ul class="classifiers-list">
                <li class="classifiers-list__item">
                    <span>&bull;</span>
                    <a class="link classifiers-list__link" href="{{ this.createUrl('classifiers/redaction1') }}">
                        Общероссийский классификатор видов экономической деятельности (ОК 029-2001 (КДЕС Ред. 1))
                    </a>
                </li>
                <li class="classifiers-list__item">
                    <span>&bull;</span>
                    <a class="link classifiers-list__link" href="{{ this.createUrl('classifiers/redaction2') }}">
                        Общероссийский классификатор видов экономической деятельности 2 (ОК 029-2014 (КДЕС Ред. 2))
                    </a>
                </li>
                <li class="classifiers-list__item">
                    <span>&bull;</span>
                    <a class="link classifiers-list__link" href="{{ this.createUrl('classifiers/training') }}">
                        Общероссийский классификатор занятий
                    </a>
                </li>
                <li class="classifiers-list__item">
                    <span>&bull;</span>
                    <a class="link classifiers-list__link" href="{{ this.createUrl('classifiers/prof') }}">
                        Общероссийский классификатор профессий рабочих, должностей служащих
                    </a>
                </li>
                <li class="classifiers-list__item">
                    <span>&bull;</span>
                    <a class="link classifiers-list__link" href="{{ this.createUrl('classifiers/countries') }}">
                        Общероссийский классификатор стран мира
                    </a>
                </li>
                <li class="classifiers-list__item">
                    <span>&bull;</span>
                    <a class="link classifiers-list__link" href="{{ this.createUrl('classifiers/legals') }}">
                        Классификатор организационно-правовых форм
                    </a>
                </li>
                <li class="classifiers-list__item">
                    <span>&bull;</span>
                    <a class="link classifiers-list__link" href="{{ this.createUrl('classifiers/subjects') }}">
                        Классификатор субъектов Российской Федерации
                    </a>
                </li>
            </ul>
        </div>
    </div>
{% endblock %}