{% extends 'layouts/default.tpl' %}
{% block pageTitle %}PROFI.RU – Ошибка {{ error.code }}{% endblock %}
{% block pageClass %}error{% endblock %}
{% block content %}
    <div class="wrapper404">
        <h1 class="titleH1">
            Ошибка {{ error.code }}.
            {% if error.code == '400' %}
                Неверный запрос
            {% elseif error.code == '401' %}
                Необходима авторизация
            {% elseif error.code == '403' %}
                Доступ запрещен
            {% elseif error.code == '404' %}
                Страница не найдена
            {% elseif error.code == '405' %}
                Метод не поддерживается
            {% elseif error.code == '408' %}
                Время ожидания истекло
            {% endif %}
        </h1>
        {% if error.code == '404' %}
            <p>Страница не может быть загружена. Возможно, её никогда и не было, или она была переименована, или, чего доброго, удалена.</p>
            <p>Проверьте на всякий случай адрес страницы&nbsp;&mdash; вдруг в него закралась опечатка.</p>
        {% else %}
            <p>Страница не может быть загружена.</p>
        {% endif %}

        {% if error.errorCode %}
        <p>Код ошибки: <strong>{{ error.errorCode }}</strong></p>
        {% endif %}

        <a href="/" class="btn-big">Вернуться на главную</a>

        {% if common.page.env == 'dev' %}
            <div class="error__message">{{ error.message }}</div>
        {% endif %}
    </div>
{% endblock %}
