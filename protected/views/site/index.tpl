{% extends 'layouts/default.tpl' %}
{% block content %}
    <div class="content l-center">
        <div class="nav-private">
            <div class="nav-private__caption">Личный кабинет</div>
            <div class="nav-private__controls">
                <div class="nav-control__user">
                    <i class="nav-icon__user"></i>
                    <a class="nav-control__link" href="{{ this.createUrl('/') }}">{{ user.name }}</a>
                </div>
                <div class="nav-control__exit">
                    <i class="nav-icon__warning"></i>
                    <a class="nav-control__link-exit" href="{{ this.createUrl('user/logout') }}">Выйти</a>
                </div>
            </div>
        </div>
        <div class="content">
            <ul class="breadcrumbs-services">
                <li class="breadcrumbs-services__item">
                    <a class="breadcrumbs-services__item-link-current" href="/">Популярное <i>(8)</i></a>
                </li>
                <li class="breadcrumbs-services__item-delimiter">/</li>
                <li class="breadcrumbs-services__item">
                    <a class="breadcrumbs-services__item-link" href="{{ this.createUrl('site/all') }}">Все сервисы <i>(26)</i></li>
                </li>
            </ul>

            <div class="services">
            <div class="services__item">
                <div class="service-item__image-wrapper">
                    <div class="service-item__image service-item__image-1"></div>
                </div>
                <a class="service-item__link" href="http://{{ user.login }}:{{ user.password }}@192.168.0.30:8080/index.php?mod=view_queue" target="_blank">Сводка приема работодателей</a>
            </div>
            <div class="services__item">
                <div class="service-item__image-wrapper">
                    <div class="service-item__image service-item__image-2"></div>
                </div>
                <a class="service-item__link"<a href="http://{{ user.login }}:{{ user.password }}@192.168.0.30:8080/index.php?mod=view_workplace_region" target="_blank">Перечень рабочих мест</a>
            </div>
            <div class="services__item">
                <div class="service-item__image-wrapper">
                    <div class="service-item__image service-item__image-3"></div>
                </div>
                <a class="service-item__link" href="http://{{ user.login }}:{{ user.password }}@192.168.0.30:8080/index.php?mod=add_person" target="_blank">Добавить работодателя</a>
            </div>
            <div class="services__item">
                <div class="service-item__image-wrapper">
                    <div class="service-item__image service-item__image-4"></div>
                </div>
                <a class="service-item__link" href="http://{{ user.login }}:{{ user.password }}@192.168.0.30:8080/index.php?mod=edit_person" target="_blank">Внести изменения в информацию<br> о работодателе</a>
            </div>
            <div class="services__item">
                <div class="service-item__image-wrapper">
                    <div class="service-item__image service-item__image-5"></div>
                </div>
                <a class="service-item__link" href="http://{{ user.login }}:{{ user.password }}@192.168.0.30:8080/index.php?mod=view_person" target="_blank">Просмотр информации о работодателе</a>
            </div>
            <div class="services__item">
                <div class="service-item__image-wrapper">
                    <div class="service-item__image service-item__image-6"></div>
                </div>
                <a class="service-item__link" href="http://{{ user.login }}:{{ user.password }}@192.168.0.30:8080/index.php?mod=mvk_notes" target="_blank">Выписки из протокола МВК</a>
            </div>
            <div class="services__item">
                <div class="service-item__image-wrapper">
                    <div class="service-item__image service-item__image-7"></div>
                </div>
                <a class="service-item__link" href="http://{{ user.login }}:{{ user.password }}@192.168.0.30:8080/index.php?mod=edit_queue" target="_blank">Редактирование времени приема</a>
            </div>
            <div class="services__item">
                <div class="service-item__image-wrapper">
                    <div class="service-item__image service-item__image-8"></div>
                </div>
                <a class="service-item__link" href="http://{{ user.login }}:{{ user.password }}@192.168.0.30:8080/index.php?mod=news_admin" target="_blank">Редактирование новостей</a>
            </div>
        </div>
        </div>
    </div>

    <div class="content l-center">
        <div class="content-box">
            {% if news %}
                <div class="news">
                    <div class="news__caption">Новости</div>
                    <ul class="news-list">
                        {% for newsItem in news %}
                        <li class="news-item">
                            <span class="news-item__date">{{ newsItem.date }}</span>
                            <a class="news-item__text" href="{{ this.createUrl('news/item', {'id' : newsItem.id}) }}">{{ newsItem.title }}</a>
                        </li>
                        {% endfor %}
                    </ul>
                    <a href="{{ this.createUrl('news/index') }}" class="link news__link-all">Архив новостей</a>
                </div>
            {% endif %}


            <ul class="links-list">
                <li class="links-list__item_orange-border">
                    <a class="link links-list__item-link" href="http://192.168.0.30:8080/index.php?mod=presentation">Презентации</a>
                </li>
                <li class="links-list__item">
                    <a class="link links-list__item-link" href="http://192.168.0.30:8080/index.php?mod=view_journal">Электронный журнал</a>
                </li>
                <li class="links-list__item">
                    <a class="link links-list__item-link" href="/subjects/">Уполномоченные органы субъектов РФ</a>
                </li>
                <li class="links-list__item">
                    <a class="link links-list__item-link" href="/instructions/">Иструкции по работе с АИК «Миграционные
                        квоты»</a>
                </li>
                <li class="links-list__item">
                    <a class="link links-list__item-link" href="/immigrationLegislation/">Миграционное законодательство</a>
                </li>
                <li class="links-list__item">
                    <a class="link links-list__item-link" href="/about/">Задачи и функции АИК «Миграционные квоты»</a>
                </li>
                <li class="links-list__item">
                    <a class="link links-list__item-link" href="/classifiers/">Классификаторы</a>
                </li>
            </ul>
        </div>
    </div>
{% endblock %}