{% extends 'layouts/default.tpl' %}

{% block content %}
    <div class="content_inner l-center">
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a class="breadcrumbs-item__link" href="/">Главная</a>
            </li>
            <li class="breadcrumbs-item">&#8594;</li>
            <li class="breadcrumbs-item">
                <a class="breadcrumbs-item__link" href="{{ this.createUrl('user/login') }}">Вход в ЛК</a>
            </li>
            <li class="breadcrumbs-item">&#8594;</li>
            <li class="breadcrumbs-item breadcrumbs-item_current">Связь с центром АИК</li>
        </ul>

        <div class="support-area-static">
            {{ html.form('', 'post', {class: 'form form-support'})|raw }}
                <div class="caption-xl">Связаться с центром администрирования АИК</div>
                <div class="form-group">
                    <label class="form-group__label form-group__label-m" for="lf-login">Наименование организации, ФИО физического лица:</label>
                    <input class="form-group__field form-group__field-xl" type="text" id="lf-login" name="SupportForm[login]"/>
                </div>
                <div class="form-group">
                    <label class="form-group__label form-group__label-m" for="lf-login">ИНН:</label>
                    <input class="form-group__field form-group__field-xl" type="text" id="lf-login" name="SupportForm[login]"/>
                </div>
                <div class="form-group">
                    <label class="form-group__label form-group__label-m" for="lf-login">Ф.И.О. Контактного лица:</label>
                    <input class="form-group__field form-group__field-xl" type="text" id="lf-login" name="SupportForm[login]"/>
                </div>
                <div class="form-group">
                    <label class="form-group__label form-group__label-m" for="lf-login">Телефон:</label>
                    <input class="form-group__field form-group__field-xl" type="text" id="lf-login" name="SupportForm[login]"/>
                </div>
                <div class="form-group">
                    <label class="form-group__label form-group__label-m" for="lf-login">E-mail адрес:</label>
                    <input class="form-group__field form-group__field-xl" type="text" id="lf-login" name="SupportForm[login]"/>
                </div>

                <div class="form-group">
                    <label class="form-group__label form-group__label-m" for="lf-login">Регион, из которого будет отправлено сообщение: :</label>
                    <input class="form-group__field form-group__field-xl" type="text" id="lf-login" name="SupportForm[login]"/>
                </div>

                <div class="form-group">
                    <textarea class="form-group__field form-group__field-area"></textarea>
                </div>
                <div class="form-group">
                    <input class="link form-group-submit__button form-group-submit__button-xl" type="submit" value="Отправить заявку"/>
                </div>
            {{ html.endForm()|raw }}
        </div>
    </div>
{% endblock %}