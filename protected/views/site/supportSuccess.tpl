{% extends 'layouts/default.tpl' %}

{% block content %}
<div class="content_inner l-center">

    <div class="support-success">
        <div class="caption-xl">Ваша заявка отправлена!</div>
        <a href="/" class="link back-to-main">Перейти на главную страницу</a>
    </div>
</div>
{% endblock %}