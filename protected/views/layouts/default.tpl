<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>АИК &laquo;Миграционные квоты&raquo;</title>
        <meta name="google" content="notranslate">
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
        <meta http-equiv="Content-Language" content="ru_RU">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link rel="stylesheet" href="/assets/build/css/app.vendor.css?v={{ common.staticVersion }}" type="text/css"/>
        <link rel="stylesheet" href="/assets/build/css/app.css?v={{ common.staticVersion }}" type="text/css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="header l-center">
                <div class="support support_col-l">
                    <div class="support-bar">
            <span class="support-bar__caption">
                Служба поддержки:
            </span>
                        <a href="#feedback" class="support-bar__item support-bar__item_email">
                            support@adapt.ru
                        </a>
            <span class="support-bar__comments">
                (круглосуточно)
            </span>
                    </div>
                    <div class="support-bar__delimiter"></div>
                    <div class="support-bar">
                        <span class="phone-link">+7 (929) 919-72-08</span>
                    </div>
                    <div class="support-bar__delimiter"></div>
                    <div class="support-bar">
                        <a href="#feedback" class="link__feedback">Написать письмо</a>
                    </div>
                </div>
            </div>
            <div class="content l-center">
                <div class="content-head">
                    <a href="/" class="link__header">
                        <h2 class="link__header_caption-m">Министерство труда и социальной защиты Российской Федерации</h2>

                        <div class="link__header_logo"></div>
                        <h1 class="link__header_caption-l">&laquo;Миграционные квоты&raquo;</h1>

                        <h3 class="link__header_caption-xs">автоматизированный информационный комплекс</h3>
                    </a>
                </div>
            </div>
            {% block content %}{% endblock %}
        </div>
        <div class="wrapper-footer">
            <div class="footer content l-center">
                <a href="http://www.rosmintrud.ru" target="_blank" class="link footer__min-trud-logo"></a>
                <div class="footer__info">
                    <div class="support support_col-r">
                        <div class="support-bar">
                    <span class="support-bar__caption">
                        Служба поддержки:
                    </span>
                            <a href="#feedback" class="support-bar__item support-bar__item_email">
                                support@adapt.ru
                            </a>
                    <span class="support-bar__comments">
                        (круглосуточно)
                    </span>
                        </div>
                        <div class="support-bar__delimiter-wide"></div>
                        <div class="support-bar">
                            <span class="phone-link">+7 (929) 919-72-08</span>
                        </div>
                        <div class="support-bar__delimiter-wide"></div>
                        <div class="support-bar">
                            <a href="#feedback" class="link__feedback">Написать письмо</a>
                        </div>
                    </div>
                    <div class="copyrights">
                        <p class="copyrights__text">
                            &copy;2014-2015, Creative Common Attribution 3.0<br/>
                            Министерство труда и социальной защиты Российской Федерации<br/>
                            Все права на материалы, находящиеся на сайте, охраняются в соответствии с<br/>
                            законодательством РФ, в том числе, об авторском праве и смежных правах.
                        </p>
                        <a href="http://adapt.ru" target="_blank" class="copyrights__logo-adapt"></a>
                    </div>
                </div>
            </div>
        </div>

        {% include '/layouts/modals/feedback-modal.tpl' %}
    </body>
    <script src="/assets/build/js/app.vendor.js?v={{ common.staticVersion }}"></script>
    <script src="/assets/build/js/app.js?v={{ common.staticVersion }}"></script>
</html>