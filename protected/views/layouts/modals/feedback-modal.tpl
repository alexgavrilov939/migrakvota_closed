<div class="remodal modal-l widget-px" data-remodal-id="feedback" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc" data-widget="widgets.feedback-form">
    <button data-remodal-action="close" class="remodal-close close-button_right" aria-label="Close"></button>

    {{ html.form('', 'post', {class: 'feedback-form'})|raw }}
    <div class="caption-l">Написать письмо</div>
    <span class="error-msg" style="display: none;">Проверьте правильность введенных данных!</span>
    <div class="form-group">
        <label class="form-group__label form-group__label-m" for="fb-reason">Причина обращения:</label>
        <input class="form-group__field form-group__field-l" type="text" id="fb-reason" name="FeedbackForm[reason]"/>
    </div>
    <div class="form-group">
        <label class="form-group__label form-group__label-m" for="fb-fio">Наименование/ФИО:</label>
        <input class="form-group__field form-group__field-l" type="text" id="fb-fio" name="FeedbackForm[fio]"/>
    </div>
    <div class="form-group">
        <label class="form-group__label form-group__label-m" for="fb-email">Email:</label>
        <input class="form-group__field form-group__field-l" type="text" id="fb-email" name="FeedbackForm[email]"/>
    </div>
    <div class="form-group">
        <textarea class="form-group__field form-group__field-area_m" id="fb-body" name="FeedbackForm[body]"></textarea>
    </div>

    <div class="form-group">
        <input type="submit" class="link form-group-submit__button form-group-submit__button-l js-feedback-submit" value="Отправить"/>
    </div>
    <!-- <button data-remodal-action="cancel" class="remodal-cancel">Закрыть</button>
     <button data-remodal-action="confirm" class="remodal-confirm">Войти</button>
     <input type="submit" class="login login-submit" value="Войти"/>-->
    {{ html.endForm()|raw }}

    <div class="form-feedback-success caption-l" style="display: none;">
        Заявка отправлена!
    </div>
</div>