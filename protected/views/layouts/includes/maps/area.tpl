<div class="maps">
    <div class="maps__inner">
        <div class="maps__caption">Выберите регион</div>
        <div id="map" class="widget-px" data-widget="widgets.regions-map"></div>
    </div>
    <div class="map__result-data js-mapResultData">
        {% include '/layouts/includes/maps/region-info.tpl' %}
    </div>
</div>