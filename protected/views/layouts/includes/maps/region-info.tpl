{% if region.fullname %}
<div class="map__caption">{{ region.fullname|capitalize }}</div>
{% endif %}

<ul class="map-list">
    {% if region.address %}
    <li class="map-list__item">
        <div class="map-list__item-name">Адрес:</div>
        <div class="map-list__item-value">{{ region.address }}</div>
    </li>
    {% endif %}
    {% if region.tel %}
    <li class="map-list__item">
        <div class="map-list__item-name">Телефон:</div>
        <div class="map-list__item-value">{{ region.tel }}</div>
    </li>
    {% endif %}
    {% if region.mail %}
    <li class="map-list__item">
        <div class="map-list__item-name">Email:</div>
        <div class="map-list__item-value map-list__item-value-email">
            <a href="mailto:{{ region.mail }}">{{ region.mail }}</a>
        </div>
    </li>
    {% endif %}
    {% if region.site %}
    <li class="map-list__item">
        <div class="map-list__item-name">Сайт:</div>
        <div class="map-list__item-value map-list__item-value-site">
            <a href="{{ region.site }}" target="_blank">{{ region.site }}</a>
        </div>
    </li>
    {% endif %}
</ul>

{% if region.recivetime %}
<div class="map__caption">Дни и часы приема заявок:</div>
<span class="map__item-value">{{ region.recivetime|capitalize }}</span>
{% endif %}

{% if region.person %}
<div class="map__caption">Уполномоченные сотрудники:</div>
<ul class="map-list">
    <li class="map-list__item">
        <div class="map-list__item-name">ФИО:</div>
        <div class="map-list__item-value">{{ region.person.lname }} {{ region.person.fname }} {{ region.person.pname }}</div>
    </li>
    <li class="map-list__item">
        <div class="map-list__item-name">Должность:</div>
        <div class="map-list__item-value">{{ region.person.position|capitalize }}</div>
    </li>
    <li class="map-list__item">
        <div class="map-list__item-name">Телефон:</div>
        <div class="map-list__item-value">{{ region.person.tel }}</div>
    </li>
    <li class="map-list__item">
        <div class="map-list__item-name">Email:</div>
        <div class="map-list__item-value  map-list__item-value-email">
            <a href="mailto:{{ region.person.mail}}">{{ region.person.mail}}</a>
        </div>
    </li>
</ul>
{% endif %}

<div class="loader"></div>