{% extends 'layouts/default.tpl' %}
{% block content %}
    <div class="content l-center">
        {{ html.form('', 'post', {class: 'form form-login'})|raw }}
            <div class="caption-l">Для зарегистрированных пользователей</div>
            {% if form.errors %}
                <div class="error-msg">Неправильный логин или пароль!</div>
            {% endif %}
            <div class="form-group">
                <label class="form-group__label" for="lf-login">Логин:</label>
                <input class="form-group__field form-group__field-l" type="text" id="lf-login" name="LoginForm[login]"/>
            </div>
            <div class="form-group">
                <label class="form-group__label" for="lf-pass">Пароль:</label>
                <input class="form-group__field form-group__field-l" type="password" id="lf-pass" name="LoginForm[password]"/>
            </div>

            <div class="form-group form-group-submit">
                <input class="link form-group-submit__button" type="submit" value="Войти"/>
            </div>

            <div class="form-group-links">
                <a class="link form-group-links__item" href="{{ this.createUrl('user/restorePass') }}">Забыли пароль?</a>
                <a class="link form-group-links__item" href="{{ this.createUrl('user/registration') }}">Зарегистрироваться</a>
                <a class="link form-group-links__item form-group-links__item-mt" href="{{ this.createUrl('site/support') }}">Связаться с Центром администрирования АИК</a>
            </div>
        {{ html.endForm()|raw }}
    </div>
{% endblock %}