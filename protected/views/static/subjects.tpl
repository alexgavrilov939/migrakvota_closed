{% extends 'layouts/default.tpl' %}
{% block content %}
<div class="content l-center">
    <ul class="breadcrumbs">
        <li class="breadcrumbs-item">
            <a class="breadcrumbs-item__link" href="/">Главная</a>
        </li>
        <li class="breadcrumbs-item">&#8594;</li>
        <li class="breadcrumbs-item breadcrumbs-item_current">Уполномоченные органы</li>
    </ul>

    <div class="caption-xl">Уполномоченные органы субъектов РФ</div>
    {% include '/layouts/includes/maps/area.tpl' %}
</div>
{% endblock %}