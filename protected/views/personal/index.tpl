{% extends 'layouts/default.tpl' %}

{% block content %}
<div class="content l-center">
    <div class="nav-private">
        <div class="nav-private__caption">Личный кабинет</div>
        <div class="nav-private__controls">
            <div class="nav-control__user">
                <i class="nav-icon__user"></i>
                <a class="nav-control__link"" href="{{ this.createUrl('personal/index') }}">
                    {% if user.contact %}
                        {{ user.contact }}
                    {% else %}
                        Администратор
                    {% endif %}
                </a>
            </div>
            <div class="nav-control__exit">
                <i class="nav-icon__warning"></i>
                <a class="nav-control__link-exit" href="{{ this.createUrl('user/logout') }}">Выйти</a>
            </div>
        </div>
    </div>

    <div class="body-private">
        <ul class="personal-nav">
            <li class="personal-nav__item personal-nav__item-current">
                <a class="personal-nav__link" href="{{ this.createUrl('personal/requestStatus') }}">
                    <div class="nav-image__wrapper">
                        <div class="count-sticker">{{ requestData.total }}</div>
                        <div class="nav-image__wrapper_image personal-nav__item-1"></div>
                    </div>
                </a>
            </li>
            <li class="personal-nav__item">
                <a class="personal-nav__link" href="{{ this.createUrl('personal/getQueueInfo') }}">
                    <div class="nav-image__wrapper">
                        <div class="nav-image__wrapper_image personal-nav__item-2"></div>
                    </div>
                </a>
            </li>
            <li class="personal-nav__item">
                <a class="personal-nav__link" href="{{ this.createUrl('personal/changeContactInfo') }}">
                    <div class="nav-image__wrapper">
                        <div class="nav-image__wrapper_image personal-nav__item-3"></div>
                    </div>
                </a>
            </li>
        </ul>
        <div class="personal-content__inner">
            <div class="caption-ml">Статус заявок</div>

            <div class="warning-blocks">
                <div class="warning-blocks__item">
                    При направлении на рассмотрение новой заявки, либо внесений в существующую заявку и последующего
                    повторного отправления на рассмотрение,
                    не забывайте направлять подписанный бумажный экземпляр заявки в уполномоченный орган региона, в
                    который вы подаете заявку.
                    Все заявки, для которых не был предоставлен подписанный экземпляр, будут автоматически отклонены.
                </div>
                <div class="warning-blocks__item">
                    Вы можете отправить следующую заявку на корректировку квоты в один регион после того,
                    как предыдущая заявка в этот регион была включена в квоту или отклонена.
                </div>
                <div class="warning-blocks__item">
                    Вы можете оознакомиться с опубликованными приказами Министерства труда и социальной защиты по
                    ссылке.
                </div>
            </div>

            <table class="docs-table">
                <thead>
                <tr class="docs-table__tr docs-table__thead-tr">
                    <td class="docs-table__tr-td docs-table__thead-tr-td">Год</td>
                    <td class="docs-table__tr-td docs-table__thead-tr-td">Регион</td>
                    <td class="docs-table__tr-td docs-table__thead-tr-td">Тип</td>
                    <td class="docs-table__tr-td docs-table__thead-tr-td">Статус</td>
                    <td class="docs-table__tr-td docs-table__thead-tr-td">Изменить статус</td>
                    <td class="docs-table__tr-td docs-table__thead-tr-td">ЭЦП</td>
                </tr>
                </thead>
                <tbody>

                {% for item in requestData.records  %}
                    {% set type = item.type == 'prim' ? 'Основная заявка' : 'Корректировка квоты' %}
                    {% if item.type == 'prim' %}
                        {% set type = 'Основная заявка' %}
                    {% else %}
                        {% set type = 'Корректировка квоты ' %}
                        {% if item.increase %}
                            {% set type = type ~ '(увеличение)' %}
                        {% else %}
                            {% set type = type ~ '(уменьшение)' %}
                        {% endif %}
                    {% endif %}
                    <tr class="docs-table__tr ">
                        <td class="docs-table__tr-td">{{ item.year }}</td>
                        <td class="docs-table__tr-td">{{ item.regionfull.fullname }}</td>
                        <td class="docs-table__tr-td">{{ type }}</td>
                        <td class="docs-table__tr-td">{{ item.statusfull.name }}</td>
                        <td class="docs-table__tr-td">
                            {% if item.statusfull.action_person == 'recall' %}
                                {% set buttonName = 'Отозвать' %}
                                {% set link = this.createUrl('personal/declineRequest', {'reqId' : item.id}) %}
                            {% elseif item.year == 2016 %}
                                {% set buttonName = 'Изменить' %}
                                {% set link = this.createUrl('personal/changeRequestStatus', {'reqId' : item.id}) %}
                            {% else  %}
                                {% set buttonName = null %}
                            {% endif %}

                            {% if buttonName is not null %}
                                <a class="personal-request__withdraw" href="{{ link }}">{{ buttonName }}</a>
                            {% endif %}
                        </td>
                        <td class="docs-table__tr-td">
                            {% if item.status == 'sent' or item.status == 'sent_after_recall' %}
                                <a class="personal-request__sign" href="#">Подписать</a>
                            {% endif %}
                        </td>
                    </tr>
                {% endfor %}
                </tbody>
            </table>
        </div>
    </div>
</div>


{% endblock %}