{% extends 'layouts/default.tpl' %}
{% block content %}
    <div class="content_inner l-center">
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a class="breadcrumbs-item__link" href="/">Главная</a>
            </li>
            <li class="breadcrumbs-item">&#8594;</li>
            <li class="breadcrumbs-item breadcrumbs-item_current">Новости</li>
        </ul>

        <div class="news__caption caption-xl">Новости</div>
        {% if news %}
            <div class="news-xl">
                <ul class="news-list-xl">
                    {% for newsItem in news %}
                    <li class="news-item news-item-xl">
                        <span class="news-item__date-xl">{{ newsItem.date }}</span>
                        <a class="news-item__text news-item__text-xl" href="{{ this.createUrl('news/item', {'id' : newsItem.id}) }}">{{ newsItem.title }}</a>
                    </li>
                    {% endfor %}
                </ul>
            </div>
        {% endif %}

        {% if pager %}
            <ul class="paginator paginator-news">
                {% if pager.page > 1 %}
                    <li class="paginator__item">
                        <a class="link paginator__item-link" href="{{ pager.prevUrl }}">&laquo; Назад</a>
                    </li>
                {% endif %}
                <li class="paginator__item">
                    <a class="link paginator__item-link" href="{{ pager.nextUrl }}">Вперед &raquo;</a>
                </li>
            </ul>
        {% endif %}
    </div>
{% endblock %}