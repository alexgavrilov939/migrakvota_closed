{% extends 'layouts/default.tpl' %}

{% block content %}
    <div class="content_inner l-center">
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a class="breadcrumbs-item__link" href="/">Главная</a>
            </li>
            <li class="breadcrumbs-item">&#8594;</li>
            <li class="breadcrumbs-item">
                <a class="breadcrumbs-item__link" href="{{ this.createUrl('news/index') }}">Новости</a>
            </li>
            <li class="breadcrumbs-item">&#8594;</li>
            <li class="breadcrumbs-item breadcrumbs-item_current">{{ title }}</li>
        </ul>

        <div class="caption-xl">{{ newsItem.title }}</div>

        <div class="content content-inline l-center">
            {% if newsItem.text %}
                {{ newsItem.text|raw }}
            {% else %}
                Новость не найдена!
            {% endif %}
        </div>
    </div>
{% endblock %}