<?php
// Encoding
setlocale(LC_TIME, 'ru_RU.UTF-8');
mb_internal_encoding('utf-8');
date_default_timezone_set('Europe/Moscow');

// Yii constants
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 0);

Yii::setPathOfAlias('config', __DIR__);

// composer lib include
require(__DIR__ . '/../vendor/autoload.php');

// loading wrappers
require(__DIR__ . '/globals.php');

// Main config
$mainConfig = array(
    'id' => 'migrakvota_open',
    'name' => 'migrakvota web',
    'basePath' => __DIR__ . '/../',
    'language' => 'ru',
    'charset' => 'utf-8',
    'timeZone' => 'Europe/Moscow',
    'preload' => array('log', 'session', 'request'),

    'import' => array(
        'application.components.*',
        'ext.*',
        'ext.abstract.*',
        'ext.TwigViewBlocks.*',
        'application.models.*',
        'application.models.classifiers.*',
    ),

    'components' => array(
        'site' => array(
            'class' => 'SiteService'
        ),

        'viewBlocks' => array(
            'class' => 'TwigViewBlocks',
            'scriptsPosition' => CClientScript::POS_END,
        ),

        'viewRenderer' => array(
            'class' => 'TwigViewRenderer',
            'twigPathAlias' => 'application.vendor.twig.twig.lib.Twig',
            'fileExtension' => '.tpl',
            'options' => array(
                'autoescape' => true,
                'debug' => YII_DEBUG,
            ),
            'globals' => array(
                'html' => 'CHtml',
            ),
            'functions' => array(
                'plural' => 'TextHelper::plural',
                'str_replace' => 'str_replace',
                'date' => 'date'
            ),
            'filters' => array(
            ),
            'extensions' => array(
                'Twig_Extension_Debug',
            ),
        ),

        'memcache' => array(
            'class' => 'ext.MemcachedPool',
            'servers' => array(
                'localhost' => array('host' => 'localhost', 'port' => 11211),
            ),
        ),

//        'db' => array(
//            'class' => 'CDbConnection',
//            'connectionString' => 'mysql:host=82.146.43.111;port=3306;dbname=migr12',
//            'username' => 'public_user',
//            'password' => 'Y23m9HeZ',
//            'emulatePrepare' => true,
//            'charset' => 'utf8',
//            'schemaCachingDuration' => 3600,
//            'enableProfiling' => YII_DEBUG,
//            'enableParamLogging' => YII_DEBUG,
//            'autoConnect' => false,
//        ),

        'db' => array(
            'connectionString' => 'pgsql:host=82.146.43.111;port=5432;dbname=migr12',
            'username' => 'public_user',
            'password' => 'Y23m9HeZ',
            'charset' => 'utf8',
        ),


            'request' => array(
            'class' => 'Request',
        ),

        'user' => array(
            'class' => 'WebUser',
            'allowAutoLogin' => true,
//            'loginUrl' => array('user/login')
            'loginUrl' => 'http://192.168.0.30/login/'
        ),

        'urlManager'=>array(
            'urlFormat'=>'path',
            'urlSuffix' => '/',
            'showScriptName' => false,
            'caseSensitive' => false,
            'useStrictParsing' => false,
            'rules' => require(__DIR__.'/routes.php'),
        ),

        'errorHandler' => array(
            'class' => 'ErrorHandler',
            'errorAction' => 'site/error',
        ),
    ),
    'params' => require(__DIR__ . '/params.php')
);

$envConfig = require(__DIR__.'/env/'. APP_ENV . '.php');
if (is_callable($envConfig)) {
    return $envConfig($mainConfig);
} else {
    return CMap::mergeArray($mainConfig, $envConfig);
}


