<?php
/**x
 * Таблица Yii роутинга
 * См доку: http://www.yiiframework.com/doc/guide/1.1/en/topics.url#user-friendly-urls
 * По-умолчанию у урлов суффикс /
 */
return array(
    'login' => 'user/login',
    'restorePass' => 'user/restorePass',
    'registration' => 'user/registration',

    'personal' => 'personal/index',

    'feedbackForm' => 'site/feedbackForm',

    'all' => 'site/all',

    'support' => 'site/support',
    'supportSuccess' => 'site/supportSuccess',

    'about' => 'site/about',

    'immigrationLegislation' => 'static/immigrationLegislation',
    'instructions' => 'static/instructions',
    'subjects' => 'static/subjects',

    'news' => 'news/index',
    'news/<id:\d+>' => 'news/item'
);

