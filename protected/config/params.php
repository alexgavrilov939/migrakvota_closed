<?php

$staticVersion = date('Ymd');
$staticFile = __DIR__.'/../../htdocs/assets/build/js/app.js';
if (is_file($staticFile)) {
    $staticVersion = filemtime($staticFile);
}

return array(
    'staticVersion' => $staticVersion,
    'regions' => require(dirname(__DIR__) . '/data/regions.php')
);

