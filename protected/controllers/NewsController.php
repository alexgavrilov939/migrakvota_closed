<?php

class NewsController extends Controller {

    public function actionIndex()
    {
        $pager = $this->getPager();
        $this->render('index', array(
            'news' => NewsModel::getNewsCached($pager['limit'], $pager['offset']),
            'pager' => $pager
        ));
    }

    public function actionItem($id)
    {
        $item = NewsModel::getNewsById($id);
        $this->render('newsItem', array(
            'newsItem' => NewsModel::getNewsById($id),
            'title' =>  $this->getPreparedTitle($item['title'])
        ));
    }

    private function getPreparedTitle($title)
    {
        $preparedTitle = $title;
        if (mb_strlen($title) > 130) {
            $preparedTitle = mb_substr($title, 0, 80) . '...';
        }

        return $preparedTitle;
    }

    private function getPager($limit = null)
    {
        $limit = !is_null($limit) ? $limit : 15;
        $route = $this->id . '/' . $this->action->id;
        $page = Yii::app()->request->getQuery('p', 1);
        $pager = array(
            'limit' => $limit,
            'offset' => ($page - 1) * $limit,
            'page' => $page
        );

        $params['p'] = $page + 1;
        $pager['nextUrl'] = $this->createUrl($route, $params);
        if ($page - 1 > 0) {
            $params['p'] = $page - 1;
            $pager['prevUrl'] = $this->createUrl($route, $params);
        }
        unset($params['p']);
        $pager['firstUrl'] = $this->createUrl($route, $params);

        return $pager;
    }
}