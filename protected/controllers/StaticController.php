<?php

class StaticController extends Controller {

    public function actionImmigrationLegislation()
    {
        $this->render('immigrationLegislation');
    }

    public function actionInstructions()
    {
        $this->render('instructions');
    }

    public function actionSubjects()
    {
        $this->render('subjects', array(
            'region' => RegionsModel::getRegionById(45000)
        ));
    }
}