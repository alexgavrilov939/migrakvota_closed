<?php

class SiteController extends Controller {

    public function actionIndex()
    {
        $this->render('index', array(
            'user' => $this->getUserData(),
            'news' => NewsModel::getNewsCached(3),
            'region' => RegionsModel::getRegionById(45000)
        ));
    }

    public function actionAll()
    {
        $this->render('all', array(
            'user' => $this->getUserData(),
            'news' => NewsModel::getNewsCached(3),
            'region' => RegionsModel::getRegionById(45000)
        ));
    }

    private function getUserData()
    {
        return array(
            'login' => Yii::app()->user->login,
            'password' => Yii::app()->user->password,
            'name' => Yii::app()->user->userName
        );
    }

    public function actionError()
    {
        $this->renderJson(Yii::app()->errorHandler->getError());
        $this->render('error', array(
                'error' => Yii::app()->errorHandler->getError())
        );
    }

    public function actionFeedbackForm()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(400, 'invalid request type', 1404910581);
        }

        $resp = array(
            'errors' => array(),
            'status' => false
        );

        if (isset($_POST['FeedbackForm'])) {
            Yii::import('application.models.forms.FeedbackForm');
            $form = new FeedbackForm();
            $form->attributes = $_POST['FeedbackForm'];

            if ($form->validate()) {
                $resp = array('status' => true);
            } else {
                $resp = array('errors' => $form->errors);
            }
        }

        $this->renderJson($resp);
    }

    public function actionSupport()
    {
        if (isset($_POST['SupportForm'])) {
            Yii::app()->user->setState('send_request', true);
            $this->redirect($this->createUrl('site/supportSuccess'));
        }

        $this->render('support');
    }

    public function actionSupportSuccess()
    {
        if (!Yii::app()->user->getState('send_request')) {
            throw new CHttpException(400, 'invalid request type', 1404910581);
        }
        $this->render('supportSuccess');
    }

    public function actionAbout()
    {
        $this->render('about');
    }
}