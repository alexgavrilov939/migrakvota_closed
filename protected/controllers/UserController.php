<?php

class UserController extends Controller {

    public function actionLogin()
    {
        Yii::import('application.models.forms.UserLoginForm');
        $form = new UserLoginForm();
        if (isset($_POST['LoginForm'])) {
            $form->attributes = $_POST['LoginForm'];

            if ($form->validate()) {
                $successUrl = $this->createUrl('personal/index');
                $this->redirect($successUrl);
            }
        }


        $this->render('login', array(
            'form' => $form
        ));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect('http://log:out@' . Yii::app()->request->getHostName());
    }

    public function actionRestorePass()
    {
        $this->redirect($this->tmpUrl . 'index.php?mod=retrivepass');
    }

    public function actionRegistration()
    {
        $this->redirect($this->tmpUrl . 'index.php?mod=register');
    }
}