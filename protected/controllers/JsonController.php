<?php

class JsonController extends Controller {

    public function actionLogin()
    {
        $this->render('login');
    }

    public function actionGetMapData()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(400, 'invalid request type', 1404910578);
        }

        $regions = Yii::app()->params['regions'];
        $this->renderJson($regions);
    }

    public function actionGetRegionById($regionId)
    {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(400, 'invalid request type', 1404910579);
        }

        $region = RegionsModel::getRegionById($regionId);
        $this->renderJson(array(
            'html' => $this->renderPartial('/layouts/includes/maps/region-info', array(
                'region' => $region
            ), true)
        ));
    }
}