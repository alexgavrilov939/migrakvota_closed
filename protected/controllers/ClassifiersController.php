<?php

class ClassifiersController extends Controller {

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionRedaction1()
    {
        $this->render('redaction1', array(
            'items' => Okved1Model::getOkvedAllCached()
        ));
    }

    public function actionRedaction2()
    {
        $this->render('redaction2', array(
            'items' => Okved2Model::getOkvedAllCached()
        ));
    }

    public function actionTraining()
    {
        $this->render('training', array(
            'items' => Okz3Model::getOkzAllCached()
        ));
    }

    public function actionProf()
    {
        $this->render('prof', array(
            'items' => Okpdtr4Model::getOkpDtrAllCached()
        ));
    }

    public function actionCountries()
    {
        $this->render('countries', array(
            'items' => Oksm5Model::getOksmAllCached()
        ));
    }

    public function actionLegals()
    {
        $this->render('legals', array(
            'items' => Opf6Model::getOpfAllCached()
        ));
    }

    public function actionSubjects()
    {
        $this->render('subjects', array(
            'items' => Subjects6Model::getSubjectsAllCached()
        ));
    }
}