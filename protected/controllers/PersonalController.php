<?php

class PersonalController extends Controller {

    public function actionIndex()
    {

        // id = 95421
        // person_id = 29126
//        $command = Yii::app()->db->createCommand('SELECT * FROM mg_request_status  limit 5');
//        $result = $command->queryAll();
//        echo '<pre>';
//        var_dump($result);die;

//        $command = Yii::app()->db->createCommand('SELECT mg.* FROM mg_request m JOIN mg_request_comment mg on mg.reqid = m.id  GROUP BY mg.reqid LIMIT 10');
//        $result = $command->queryAll();
//        echo '<pre>';
//        var_dump($result);die;
//
//        $command = Yii::app()->db->createCommand('SELECT * FROM mg_request_comment mgr limit 5');
//        $command = Yii::app()->db->createCommand('SELECT * FROM mg_request_comment mgr WHERE mgr.reqid=95421');
//        $result = $command->queryAll();
//        echo '<pre>';
//        var_dump($result);die;

        $userData = array(
            'id' => Yii::app()->user->id,
            'contact' => Yii::app()->user->contact
        );
        $requestData = RequestModel::getRequestsDataByPersonId($userData['id']);
        $this->render('index', array(
            'user' => $userData,
            'requestData' => array(
                'records' => $requestData['response'],
                'total' => $requestData['total']
            )
        ));
    }

    /**
     * Подать заявку
     */
    public function actionRequestApply()
    {
        $this->redirect($this->tmpUrl . 'index.php?mod=add_request');
    }

    /**
     * Подать корректировку
     */
    public function actionRequestAdjustment()
    {
        $this->redirect($this->tmpUrl . 'index.php?mod=add_request_correct');
    }

    /**
     * Записаться на прием
     */
    public function actionRequestEnroll()
    {
        $this->redirect($this->tmpUrl . 'index.php?mod=queue');
    }

    /**
     * Статус заявок
     */
    public function actionRequestStatus()
    {
        $this->redirect($this->tmpUrl . 'index.php?mod=request_status');
    }

    public function actionGetQueueInfo()
    {
        $this->redirect($this->tmpUrl . 'index.php?mod=queue');
    }

    public function actionChangeContactInfo()
    {
        $this->redirect($this->tmpUrl . 'index.php?mod=change_contact_info');
    }

    /**
     * Изменить статус заявки
     * @param $reqId
     */
    public function actionChangeRequestStatus($reqId)
    {
        $this->redirect($this->tmpUrl . 'index.php?mod=request_status&act=reqsend&req=' . $reqId);
    }

    /**
     * Отозвать заявку
     * @param $reqId
     */
    public function actionDeclineRequest($reqId)
    {
        $this->redirect($this->tmpUrl . 'index.php?mod=request_status&act=recall&req=' . $reqId);
    }
}