<?php

class FeedbackForm extends CFormModel
{
    public $reason;
    public $fio;
    public $email;
    public $body;

    public function rules()
    {
        return array(
            array('reason, fio, body', 'required', 'message' => 'Обязательное поле не заполнено'),
            array('email', 'email', 'allowEmpty' => false, 'message' => 'Неверный формат E-Mail'),
        );
    }
}