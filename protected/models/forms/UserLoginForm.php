<?php

class UserLoginForm extends CFormModel
{
    public $id;
    public $login;
    public $password;

    public function rules()
    {
        return array(
            array('login', 'required', 'message' => 'Введите Логин'),
            array('password', 'required', 'message' => 'Введите пароль'),
            array('password', 'validatePassword', 'message' => 'Проверьте введенные данные'),
        );
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->login || !$this->password) { return false; }

        $identity = new UserIdentity($this->login, $this->password);
        if ($identity->authenticate()) {

            Yii::app()->user->login($identity, 3600*24*7);
            Yii::app()->request->cookies['tmp_user_id'] = new CHttpCookie(
                'tmp_user_id',
                Yii::app()->user->id,
                array('expire' => time() + 14700));

            Yii::app()->request->cookies['tmp_user_login'] = new CHttpCookie(
                'tmp_user_login',
                $this->login,
                array('expire' => time() + 14700)
            );
            Yii::app()->request->cookies['tmp_user_password'] = new CHttpCookie(
                'tmp_user_password',
                $this->password,
                array('expire' => time() + 14700)
            );


            return true;
        } else {
            $this->addError($attribute, $params['message']);
            return false;
        }
    }

}