<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class RegionPersonModel extends CActiveRecord
{
    public $id;
    public $orgid;
    public $lname;
    public $fname;
    public $pname;
    public $tel;
    public $mail;
    public $position;
    public $positionstatus;
    public $username;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_r_reciveorg_persons';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }

    public static function getPersonById($id)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('region = ' . $id);
        $criteria->limit = 1;

        return self::model()->find($criteria);
    }



    public static function getPersons($limit = 1, $offset = 0)
    {
        $criteria = new CDbCriteria();
        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        $persons = self::model()->findAll($criteria);

        return $persons;
    }
}

