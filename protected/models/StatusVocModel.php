<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class StatusVocModel extends CActiveRecord
{
    public $status;
    public $action_person;
    public $name;
    public $action_region;
    public $order;
    public $edit_person;
    public $edit_region;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_request_status';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }
}