<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class RequestCommentsModel extends CActiveRecord
{

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_request_comment';
    }

    public function relations()
    {
        return array(
            'requests' => array(self::HAS_MANY, 'RequestModel', 'reqid')
        );
    }

    public static function getTotal()
    {
        return self::model()->count();
    }
}

