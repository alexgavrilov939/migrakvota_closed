<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class UsersModel extends CActiveRecord
{

    public $id;
    public $name;
    public $login;
    public $password;
    public $region;
    public $type;
    public $access_list;
    public $email;
    public $edit;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_users';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }

    public static function getUsers($limit = 1, $offset = 0)
    {
        $criteria = new CDbCriteria();
        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        $persons = self::model()->findAll($criteria);

        return $persons;
    }

    public static function getByLogin($login)
    {
        $cacheKey = 'user_data_' . md5(serialize($login));
        $user = Yii::app()->memcache->get($cacheKey);
        if ($user !== false) {
            return $user;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('login = \'' . $login . '\'');
        $user = self::model()->find($criteria);
        Yii::app()->memcache->set($cacheKey, $user, 3600);

        return $user;
    }

    public static function getPersonByLogin($login)
    {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('login', array($login));

        $persons = self::model()->find($criteria);

        return $persons;
    }


    public static function getNewsById($id)
    {
        return self::model()->findByPk($id);
    }
}

