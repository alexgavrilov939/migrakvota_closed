<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class RequestModel extends CActiveRecord
{
    public $id;
    public $person_id;
    public $year;
    public $region;
    public $template_id;
    public $status;
    public $type;
    public $in_region_req;
    public $date;
    public $add_by;
    public $import_xls;
    public $subregion;
    public $reasondecline;
    public $noticemail;
    public $incdate;
    public $increase;
    public $editdate;
    public $parent_id;
    public $flow;
    public $reasondecline2;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_request';
    }

    public function relations()
    {
        return array(
            'comment' => array(self::BELONGS_TO, 'RequestCommentsModel', '', 'on' => 'comment.reqid = t.id'),
            'regionfull' => array(self::BELONGS_TO, 'RegionsVocModel', '', 'on' => 'regionfull.id = t.region and regionfull.subid = t.subregion'),
            'statusfull' => array(self::BELONGS_TO, 'StatusVocModel', '', 'on' => 'statusfull.status = t.status')
        );
    }

    public static function getTotal()
    {
        return self::model()->count();
    }

    public static function getRequest($limit = 1, $offset = 0)
    {
        $criteria = new CDbCriteria();
        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        $persons = self::model()->findAll($criteria);

        return $persons;
    }

    public static function getRequestsDataByPersonId($personId)
    {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('person_id', array($personId));
        $criteria->order = 't.year DESC';

        $request = self::model()->with('comment', 'regionfull', 'statusfull');
        $response = $request->findAll($criteria);

//        echo '<pre>';
//        var_dump($response);die;


        return array(
            'response' => $response,
            'total' => $request->count($criteria)
        );
    }
}

