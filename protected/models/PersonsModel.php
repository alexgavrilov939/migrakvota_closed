<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class PersonsModel extends CActiveRecord
{
    public $id;
    public $name;
    public $address_j;
    public $address_f;
    public $egr;
    public $inn;
    public $director;
    public $director_tel;
    public $contact;
    public $contact_tel;
    public $contact_email;
    public $login;
    public $password;
    public $region;
    public $status;
    public $opf;
    public $contact_job;
    public $okved;
    public $date;
    public $add_by;
    public $import_xls;
    public $kpp;
    public $subregion;
    public $secretquest;
    public $secretanswer;
    public $inn_parent;
    public $inn_parent_accept;
    public $notified;
    public $ses_news_region;
    public $okved_version;
    public $egr_rules;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_persons';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }

    public static function getPersons($limit = 1, $offset = 0)
    {
        $criteria = new CDbCriteria();
        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        $persons = self::model()->findAll($criteria);

        return $persons;
    }

    public static function getPersonByLogin($login)
    {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('login', array($login));

        $persons = self::model()->find($criteria);

        return $persons;
    }


    public static function getNewsById($id)
    {
        return self::model()->findByPk($id);
    }
}

