<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class RegionsModel extends CActiveRecord
{
    public $id;
    public $name;
    public $region;
    public $fullname;
    public $address;
    public $tel;
    public $mail;
    public $directrequestrecive;
    public $recivetime;
    public $reciveorg;
    public $site;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_r_reciveorg';
    }

    public function relations()
    {
        return array(
            'person' => array(self::HAS_ONE, 'RegionPersonModel', 'id')
        );
    }

    public static function getTotal()
    {
        return self::model()->count();
    }

    public static function getRegionById($id)
    {
        $cacheKey = 'region_' . md5(serialize($id));
        $region = Yii::app()->memcache->get($cacheKey);
        if ($region !== false) {
            return $region;
        } else {
            $criteria = new CDbCriteria();
            $criteria->with = array('person');
            $criteria->addInCondition('region', array($id));
            $criteria->limit = 1;
            $region = self::model()->find($criteria);
            Yii::app()->memcache->set($cacheKey, $region, 3600);

            return $region;
        }
    }

    public static function getRegions($limit = 1, $offset = 24)
    {
        $criteria = new CDbCriteria();
        $criteria->with = array('person');

        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        return self::model()->findAll($criteria);
    }
}