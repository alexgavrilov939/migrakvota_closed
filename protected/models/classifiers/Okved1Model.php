<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class Okved1Model extends CActiveRecord
{
    public $id;
    public $name;
    public $parent_id;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_r_okved';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }


    public static function getOkved($limit = 1, $offset = 0)
    {
        $criteria = new CDbCriteria();
        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        $okved = self::model()->findAll($criteria);

        return $okved;
    }

    public static function getOkvedAllCached()
    {
        if (YII_DEBUG) {
            return self::model()->findAll();
        } else {
            $cacheKey = 'okved1_' . serialize(func_get_args());
            $okved = Yii::app()->memcache->get($cacheKey);
            if ($okved !== false) {
                return $okved;
            }
            $okved = self::model()->findAll();
            Yii::app()->memcache->set($cacheKey, $okved, 3600);

            return $okved;
        }
    }
}

