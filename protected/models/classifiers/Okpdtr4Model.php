<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class Okpdtr4Model extends CActiveRecord
{
    public $id;
    public $check_val;
    public $name;
    public $okz;
    public $class;
    public $etsk_ver;
    public $category;
    public $type;
    public $disabled;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_r_okpdtr';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }


    public static function getOkpDtr($limit = 1, $offset = 0)
    {
        $criteria = new CDbCriteria();
        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        $okved = self::model()->findAll($criteria);

        return $okved;
    }

    public static function getOkpDtrAllCached()
    {
        if (YII_DEBUG) {
            return self::getOkpDtrAll();
        } else {
            $cacheKey = 'OkpDtr3_' . serialize(func_get_args());
            $okved = Yii::app()->memcache->get($cacheKey);
            if ($okved !== false) {
                return $okved;
            }
            $okved = self::getOkpDtrAll();
            Yii::app()->memcache->set($cacheKey, $okved, 3600);

            return $okved;
        }
    }

    private static function getOkpDtrAll()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'id asc';

        return self::model()->findAll($criteria);
    }
}

