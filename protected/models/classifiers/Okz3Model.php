<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class Okz3Model extends CActiveRecord
{
    public $id;
    public $check_val;
    public $name;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_r_okz';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }

    public static function getOkz($limit = 1, $offset = 0)
    {
        $criteria = new CDbCriteria();
        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        $okz = self::model()->findAll($criteria);

        return $okz;
    }

    public static function getOkzAllCached()
    {
        if (YII_DEBUG) {
            return self::getOkzAll();
        } else {
            $cacheKey = 'okz3_' . serialize(func_get_args());
            $okz = Yii::app()->memcache->get($cacheKey);
            if ($okz !== false) {
                return $okz;
            }
            $okz = self::getOkzAll();
            Yii::app()->memcache->set($cacheKey, $okz, 3600);

            return $okz;
        }
    }

    private static function getOkzAll()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'id asc';

        return  self::model()->findAll($criteria);
    }
}

