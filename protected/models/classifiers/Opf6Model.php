<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class Opf6Model extends CActiveRecord
{
    public $id;
    public $name;
    public $description;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_r_opf';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }


    public static function getOpf($limit = 1, $offset = 0)
    {
        $criteria = new CDbCriteria();
        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        $okved = self::model()->findAll($criteria);

        return $okved;
    }

    public static function getOpfAllCached()
    {
        if (YII_DEBUG) {
            return self::getOpfAll();
        } else {
            $cacheKey = 'opf6_' . serialize(func_get_args());
            $okved = Yii::app()->memcache->get($cacheKey);
            if ($okved !== false) {
                return $okved;
            }
            $okved = self::getOpfAll();
            Yii::app()->memcache->set($cacheKey, $okved, 3600);

            return $okved;
        }
    }

    private static function getOpfAll()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'id asc';

        return self::model()->findAll($criteria);
    }
}

