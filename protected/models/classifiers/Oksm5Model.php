<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */


class Oksm5Model extends CActiveRecord
{
    public $id;
    public $name;
    public $visa;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_r_oksm';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }

    public static function getOksm($limit = 1, $offset = 0)
    {
        $criteria = new CDbCriteria();
        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        $okved = self::model()->findAll($criteria);

        return $okved;
    }

    public static function getOksmAllCached()
    {
        if (YII_DEBUG) {
            return self::getOksmAll();
        } else {
            $cacheKey = 'oksm5_' . serialize(func_get_args());
            $okved = Yii::app()->memcache->get($cacheKey);
            if ($okved !== false) {
                return $okved;
            }
            $okved = self::getOksmAll();
            Yii::app()->memcache->set($cacheKey, $okved, 3600);

            return $okved;
        }
    }

    private static function getOksmAll()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'id asc';

        return self::model()->findAll($criteria);
    }
}

