<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class RegionsVocModel extends CActiveRecord
{
    public $id;
    public $subid;
    public $name;
    public $fullname;
    public $notsel;
    public $fo_id;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_r_okato';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }
}