<?php
/**
 * User: alexgavrilov939
 * Date: 03.09.15
 */

class NewsModel extends CActiveRecord
{
    public $id;
    public $name;
    public $region;
    public $owner;
    public $date;
    public $title;
    public $text;
    public $audience;
    public $region_str;

    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    public function tableName()
    {
        return 'mg_news';
    }

    public static function getTotal()
    {
        return self::model()->count();
    }

    public static function getNewsCached($limit = 10, $offset = 0)
    {
        if (YII_DEBUG) {
            return self::getNews($limit, $offset);
        } else {
            $cacheKey = 'news_' . serialize(func_get_args());
            $news = Yii::app()->memcache->get($cacheKey);
            if ($news === false) {
                $news = self::getNews($limit, $offset);
                Yii::app()->memcache->set($cacheKey, $news, 3600);
            }

            return $news;
        }
    }

    private static function getNews($limit, $offset)
    {
        $criteria = new CDbCriteria();
        if (!is_null($offset)) {
            $criteria->offset = $offset;
        }
        if (!is_null($limit)) {
            $criteria->limit = $limit;
        }

        $criteria->addInCondition('audience', array('all', 'persons'));
        $criteria->order = 'date DESC';

        $newsRows = self::model()->findAll($criteria);
        $news = array();
        foreach ($newsRows as $newsItem) {
            if (trim(strip_tags($newsItem->text)) == '') { continue; }
            $news[] = array(
                'id' => (int)$newsItem['id'],
                'region' => (int)$newsItem['region'],
                'date' => date('d.m.Y', strtotime($newsItem['date'])),
                'title' => $newsItem['title'],
                'text' => $newsItem['text']
            );
        }

        return $news;
    }


    public static function getNewsById($id)
    {
        $cacheKey = 'news_item_' . serialize(func_get_args());
        $newsItem = Yii::app()->memcache->get($cacheKey);
        if ($newsItem == false) {
            $newsItem = self::model()->findByPk($id);
            Yii::app()->memcache->set($cacheKey, $newsItem, 3600);
        }

        return self::model()->findByPk($id);
    }
}

