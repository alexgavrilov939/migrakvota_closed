<?php

/**
 * Class SiteService
 */
class SiteService extends CApplicationComponent
{
    public function getEnv()
    {
        return defined('APP_ENV') ? APP_ENV : 'prod';
    }
}
