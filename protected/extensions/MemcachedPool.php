<?php
class MemcachedPool extends CCache
{
    /**
     * @var Memcached instance
     */
    private $_caches = null;


    public function setServers($servers)
    {
        foreach ($servers as $key => $server) {
            $mc = new CMemCache();
            $mc->useMemcached = true;
            $mc->setServers(array($server));
            $mc->init();
            $this->_caches[$key] = $mc;
        }
    }

    protected function getValue($key)
    {
        foreach ($this->_caches as $i => $cache) {
            if ($resp = $cache->get($key)) {
                return $resp;
            }
        }

        return false;
    }


    protected function setValue($key,$value,$expire)
    {
        //set value for all available memcached services
        $resp = false;
        foreach ($this->_caches as $i => $cache) {
            if (!$cache->set($key, $value, $expire)) {
                Yii::log("memcached is not available at {$i}", CLogger::LEVEL_ERROR, 'memcached.warning');
                continue;
            }

            $resp = true;
        }

        return $resp;
    }

    protected function deleteValue($key)
    {
        $resp = false;
        foreach ($this->_caches as $i => $cache) {
            if (!$cache->delete($key)) {
                Yii::log("memcached is not available at {$i}", CLogger::LEVEL_WARNING, 'memcached.warning');
                continue;
            }
            $resp = true;
        }

        return $resp;
    }
}
