<?php

class UserIdentity extends CUserIdentity
{
    private $_id;
    public $login;
    public $password;

    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;

        parent::__construct($login, $password);
    }

    public function authenticate()
    {
        $record = UsersModel::getByLogin($this->login);
        $hash = false;
        if (!is_null($record)) {
            $hash = CPasswordHelper::hashPassword($record->password);
        }

        if (!CPasswordHelper::verifyPassword($this->password, $hash)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $record['id'];
            $this->setState('userName', $record->name);
            $this->setState('login', $record->login);
            $this->setState('password', $record->password);
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}