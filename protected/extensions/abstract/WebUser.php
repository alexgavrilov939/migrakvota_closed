<?php

class WebUser extends CWebUser {

    public function getType()
    {
        $type = 'guest';
        if (!Yii::app()->user->isGuest) {
            $type = 'authorized';
        }

        return $type;
    }
}