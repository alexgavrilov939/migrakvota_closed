<?php

abstract class Controller extends CController
{
    protected $tmpUrl = 'http://192.168.0.30:8080/';

    public function beforeAction($action)
    {
        if (Yii::app()->user->isGuest) {
            if (
                !isset($_SERVER['PHP_AUTH_USER']) ||
                !isset($_SERVER['PHP_AUTH_PW'])
            ) {
                $this->showBasicAuth();
            } else {
                $login = $_SERVER['PHP_AUTH_USER'];
                $password = $_SERVER['PHP_AUTH_PW'];
                $identity = new UserIdentity($login, $password);
                if (!$identity->authenticate()) {
                    $this->showBasicAuth();
                } else {
                    Yii::app()->user->login($identity);
                    $this->redirect(Yii::app()->user->returnUrl);
                }
            }
        }

        return parent::beforeAction($action);
    }

    private function showBasicAuth()
    {
        header('WWW-Authenticate: Basic realm="migrakvota"');
        header('HTTP/1.0 401 Unauthorized');
        Yii::app()->end();
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'controllers' => array('personal'),
                'users' => array('?')
            ),
        );
    }

    public function render($view, $data=null, $return=null) {
        if (!is_array($data)) {
            $data = array();
        }
        $data['common'] = $this->getCommonData();

        if (isset($_GET['json']) && YII_DEBUG) {
            $this->renderJson($data);
        }
        return parent::render($view, $data, $return);
    }

    private function getCommonData()
    {
        $commonData = array(
            'browser' => Yii::app()->request->browser,
            'projectParams' => Yii::app()->params,
            'page' => array(
                'env' => Yii::app()->site->getEnv(),
                'controller' => $this->id,
                'action' => $this->action->id,
                'error' => Yii::app()->errorHandler->error,
            ),
            'user' => array(
                'type' => Yii::app()->user->getType()
            ),
            'request' => array(
                'cookies' => Yii::app()->request->cookies,
                'csrfToken' => Yii::app()->request->getCsrfToken(),
                'uri' => Yii::app()->request->getRequestUri(),
            ),
            'staticVersion' => Yii::app()->params['staticVersion'],
        );

        return $commonData;
    }

    /**
     * Выводит результат как json
     */
    public function renderJson($data, $end = true)
    {
        if (isset(Yii::app()->request->browser['browser']) && Yii::app()->request->browser['browser'] == 'IE' && Yii::app()->request->browser['majorver'] == '9') {
            header("Content-Type: application/json");
        } else {
            header("Content-Type: application/json; charset=".Yii::app()->charset);
        }
        echo CJSON::encode($data);
        if ($end) {
            Yii::app()->end();
        }
    }
}