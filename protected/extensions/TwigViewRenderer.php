<?php
Yii::import('application.vendor.yiiext.twig-renderer.*');

class TwigViewRenderer extends ETwigViewRenderer
{

    function init()
    {
        parent::init();
        $theme = Yii::app()->getComponent('theme');
        $basePath = $theme === null ? Yii::app()->getBasePath() : $theme->getBasePath();
        $viewPaths = array($basePath, $basePath . DIRECTORY_SEPARATOR . 'views');
        if (isset(Yii::app()->controller->module->id)) {
            $modulePath = Yii::app()->getModulePath() . '/' . Yii::app()->controller->module->id;
            $viewPaths[] = $modulePath;
            $viewPaths[] = $modulePath . DIRECTORY_SEPARATOR . 'views';
        }
        $loader = new Twig_Loader_Filesystem($viewPaths);
        $twig = $this->getTwig();
        $twig->addGlobal('app', Yii::app());
        $twig->addGlobal('this', Yii::app()->controller);
        $twig->addFunction('strftime_cased', new Twig_Function_Function(array('Format', 'strftimeCased')));
        $twig->addFunction('sprintf', new Twig_Function_Function('sprintf'));
        $twig->addFunction('ucfirst', new Twig_Function_Function(array('TextHelper', 'ucfirst')));
        $twig->addFunction('phone', new Twig_Function_Function(array('Format', 'internationalPhoneNumberToString')));
        $twig->addFunction('_block', new Twig_Function_Function('Yii::app()->viewBlocks->getTemplates'));
        $twig->addFunction('t', new Twig_Function_Function('t'));
        $twig->setLoader($loader);
    }

}
