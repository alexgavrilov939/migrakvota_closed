<?php

/**
 * Class Request
 */
class Request extends CHttpRequest
{
    const HTTP_GET = 'GET';
    const HTTP_POST = 'POST';
    const HTTP_PUT = 'PUT';
    const HTTP_DELETE = 'DELETE';

    public $checkExcludeUriPatterns = array();

    public $simpleAntiSpamTokenName = 'hshr';

    private $_filters = null;



    public function init()
    {
        parent::init();
    }

    public function getPage()
    {
        return Yii::app()->controller->id.'.'.Yii::app()->controller->action->id;
    }

    public function getHostName()
    {
        return isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : null;
    }

    public function getCookie($name, $default = null)
    {
        if (isset($this->cookies[$name])) {
            return $this->cookies[$name];
        } else {
            return $default;
        }
    }

    public function getBrowser($userAgent=null)
    {
        $userAgent = is_null($userAgent) ? Yii::app()->request->getUserAgent() :  $userAgent;
        $cacheKey = 'browscap_' . $userAgent;
        $browser = Yii::app()->memcache->get($cacheKey);
        if (!$browser && isset($_SERVER['HTTP_USER_AGENT'])) {
            $browser = 'Mozilla/5.0';
            Yii::app()->memcache->set($cacheKey, $browser, 86400);
        }
        return $browser;
    }

    public function validateCsrfToken($event)
    {
        if ($this->isPostRequest && $this->checkEnabled()) {
            try {
                parent::validateCsrfToken($event);
            } catch (\CHttpException $e){
                Yii::log('The CSRF could not be verified', CLogger::LEVEL_WARNING, 'spam.CSRF');
                throw $e;
            }
        }
    }

    /**
     * Простая проверка против спам-ботов, которые не запускают JS
     * @throws CHttpException
     * @internal param \CEvent $event event parameter
     */
    public function validateSimpleAntiSpamToken(/*$event*/)
    {
        if ($this->checkEnabled()) {
            if ($this->getIsPostRequest() || $this->getIsPutRequest()
                    || $this->getIsDeleteRequest()) {

                $method=$this->getRequestType();

                $userToken = null;
                switch($method) {
                    case 'POST':
                        $userToken = $this->getPost($this->simpleAntiSpamTokenName);
                        break;
                    case 'PUT':
                        $userToken = $this->getPut($this->simpleAntiSpamTokenName);
                        break;
                    case 'DELETE':
                        $userToken = $this->getDelete($this->simpleAntiSpamTokenName);
                }

                if ($userToken != $this->simpleAntiSpamToken()) {
                    throw new CHttpException(400,Yii::t('yii', 'The simple antispam token could not be verified.'));
                }
            }
        }
    }

    public function simpleAntiSpamToken()
    {
        return md5($this->getUserHostAddress() . $this->getUserAgent() . 'gU@YH&^%!D');
    }

    public function simpleAntiSpamTokenCode($forceAjax = false)
    {
        if ($this->isAjaxRequest || $forceAjax) {
            return sprintf(
                '<input type="hidden" name="hshr" value="%s">',
                $this->simpleAntiSpamToken()
            );
        } else {
            return sprintf("
                <script type=\"text/javascript\">
                    var v='%s';
                    document.write('<input type=\"hidden\" name=\"hshr\" value=\"'+v+'\">');
                </script>",
                $this->simpleAntiSpamToken()
            );
        }
    }

    /**
     * Возвращает элемент массива $_SERVER
     * @param $key
     * @param null $default
     * @return null
     */
    public function server($key, $default = null)
    {
        return isset($_SERVER[$key]) ? $_SERVER[$key] : $default;
    }

    /**
     */
    public function getFilters()
    {
        return $this->_filters;
    }

    /**
     * @param $key
     * @param $value
     * @internal param null $filters
     */
    public function setFilters($key, $value)
    {
        $this->_filters[$key] = $value;
    }
}